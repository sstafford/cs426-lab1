

CS 426: Homework 1
Kickstart
Background

The Red Hat kickstart documentation is available here.
Instructions

Create a kickstart file that installs CentOS-6.3-minimal-i386 without user interaction.
The following packages need to be installed: puppet, ruby, rubygems.
Use the PuppetLabs yum repository
The ntpd service should installed and running at boot.

Deliverables

Create a BitBucket repository called cs426-lab1.
Push your finished kickstart to the BitBucket repository.
Grant read access to defreez_sou.

Requirements

I will attempt to run an installation of CentOS 6.3 minimal i386 in Virtual Box. I will use the ks= parameter to start the kickstart. I should get a working CentOS installation with no further interaction.

I should be able to login with the following credentials: username: root password: Password1

The packages required in the instructions must show up with rpm -qa. The ntp service must be enabled per the output of chkconfig.

Grade: 100%
